import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Shooting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("All set. Get ready to rumble!");
        String[][] game = new String[6][6];
        for (String[] row : game) {
            Arrays.fill(row, "-");
        }

        for (int i = 0; i < game.length; i++) {
            game[i][0] = String.valueOf(i);
            game[0][i] = String.valueOf(i);
        }

        for (String[] line : game) {
            for (int i = 0; i < line.length; i++) {
                System.out.print(line[i]);
                System.out.print('|');
            }

            System.out.println();
        }

        int rowOrCol = random.nextInt(2);
        int targetCol = 0;
        int targetRow = 0;
        int target1Col = 0;
        int target2Col = 0;
        int target3Col = 0;
        int target1Row = 0;
        int target2Row = 0;
        int target3Row = 0;
        int userRowNum;
        int userColNum;
        if (rowOrCol == 0) {
            targetCol = random.nextInt(1, 6);
            target1Row = random.nextInt(1, 6);
        } else {
            target1Col = random.nextInt(1, 6);
            targetRow = random.nextInt(1, 6);
        }


        while (true) {

            if (rowOrCol == 0) {
                if (target1Row < 2) {
                    target2Row = target1Row + 1;
                    target3Row = target1Row + 2;
                } else if (target1Row > 4) {
                    target2Row = target1Row - 2;
                    target3Row = target1Row - 1;
                } else {
                    target2Row = target1Row + 1;
                    target3Row = target1Row - 1;
                }
            } else {
                if (target1Col < 2) {
                    target2Col = target1Col + 1;
                    target3Col = target1Col + 2;
                } else if (target1Col > 4) {
                    target2Col = target1Col - 2;
                    target3Col = target1Col - 1;
                } else {
                    target2Col = target1Col + 1;
                    target3Col = target1Col - 1;
                }
            }

            System.out.println("Enter row:");
            while (true) {
                if (!scanner.hasNextInt()) {
                    System.err.println("This is not a number!");
                    scanner.next();
                } else {
                    userRowNum = scanner.nextInt();
                    if (userRowNum < 1 || userRowNum > 5) {
                        System.err.println("Invalid row number! Enter a number between 1 and 5.");
                    } else {
                        break;
                    }
                }
            }

            System.out.println("Enter column:");
            while (true) {
                if (!scanner.hasNextInt()) {
                    System.err.println("This is not a number!");
                    scanner.next();
                } else {
                    userColNum = scanner.nextInt();
                    if (userColNum < 1 || userColNum > 5) {
                        System.err.println("Invalid column number! Enter a number between 1 and 5.");
                    } else {
                        break;
                    }
                }
            }

            if (rowOrCol == 0) {
                if ((userRowNum == target1Row && userColNum == targetCol) ||
                        (userRowNum == target2Row && userColNum == targetCol) ||
                        (userRowNum == target3Row && userColNum == targetCol)) {
                    System.out.println("You have hit a target!");
                    game[userRowNum][userColNum] = "×";
                    for (int i = 0; i < game.length; i++) {
                        game[i][0] = String.valueOf(i);
                        game[0][i] = String.valueOf(i);
                    }
                    for (String[] line : game) {
                        for (int i = 0; i < line.length; i++) {
                            System.out.print(line[i]);
                            System.out.print('|');
                        }
                        System.out.println();
                    }
                    boolean allTargetsHit =
                            game[target1Row][targetCol].equals("×") &&
                                    game[target2Row][targetCol].equals("×") &&
                                    game[target3Row][targetCol].equals("×");

                    if (allTargetsHit) {
                        System.out.println("Congratulations! You have hit all the targets.");
                        break;
                    }
                } else {
                    System.out.println("You have missed!");
                    game[userRowNum][userColNum] = "*";
                    for (int i = 0; i < game.length; i++) {
                        game[i][0] = String.valueOf(i);
                        game[0][i] = String.valueOf(i);
                    }

                    for (String[] line : game) {
                        for (int i = 0; i < line.length; i++) {
                            System.out.print(line[i]);
                            System.out.print('|');
                        }
                        System.out.println();
                    }
                }
            } else {

                if ((userRowNum == targetRow && userColNum == target1Col) ||
                        (userRowNum == targetRow && userColNum == target2Col) ||
                        (userRowNum == targetRow && userColNum == target3Col)) {
                    System.out.println("You have hit a target!");
                    game[userRowNum][userColNum] = "×";
                    for (int i = 0; i < game.length; i++) {
                        game[i][0] = String.valueOf(i);
                        game[0][i] = String.valueOf(i);
                    }
                    for (String[] line : game) {
                        for (int i = 0; i < line.length; i++) {
                            System.out.print(line[i]);
                            System.out.print('|');
                        }
                        System.out.println();
                    }

                    boolean allTargetsHit =
                            game[targetRow][target1Col].equals("×") &&
                                    game[targetRow][target2Col].equals("×") &&
                                    game[targetRow][target3Col].equals("×");

                    if (allTargetsHit) {
                        System.out.println("Congratulations! You have hit all the targets.");
                        break;
                    }
                } else {
                    System.out.println("You have missed!");
                    game[userRowNum][userColNum] = "*";
                    for (int i = 0; i < game.length; i++) {
                        game[i][0] = String.valueOf(i);
                        game[0][i] = String.valueOf(i);
                    }

                    for (String[] line : game) {
                        for (int i = 0; i < line.length; i++) {
                            System.out.print(line[i]);
                            System.out.print('|');
                        }
                        System.out.println();
                    }
                }
            }
        }
    }
}
